function createNewUser() {
    this.firstName = prompt('Enter your first name');
    this.lastName = prompt('Enter your last name');
    this.birthday = prompt('Enter date of your birthday', 'dd.mm.yyyy');

    this.getLogin = function () {
        alert ('Your login is: ' + `${this.firstName[0].toLowerCase() + this.lastName.toLowerCase()}`)
        // console.log(this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
    };

    this.getAge = function () {
        let elem_ofDate = this.birthday.split('.');
        let newBirthday = new Date(+elem_ofDate[2], +elem_ofDate[1], +elem_ofDate[0]);
        let today = new Date();
        let age = today.getFullYear() - newBirthday.getFullYear();
        alert('Your are ' + `${age}` + ' years old');
    };

    this.getPassword = function () {
        alert ('Your Password is: ' + `${this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6)}`)

    }
}

let newUser = new createNewUser();
console.log(newUser);
newUser.getAge();
newUser.getLogin();
newUser.getPassword();
